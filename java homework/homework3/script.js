let numFirst = +prompt('Введите первое число');
let numSecond = +prompt('Введите второе число');
let operator = prompt('Введите знак операции'); // любой: +, -, *, /} 


  function calcResult(x, y, operator) {
    switch(operator) {
      case '+':  
        return x + y
      case '-': 
        return x - y
      case '*': 
        return x * y
      case '/': 
        return x / y
      default:
        return 'error'
    }
  }
  
  const result = calcResult(numFirst, numSecond, operator);
  console.log('current result:', result)
