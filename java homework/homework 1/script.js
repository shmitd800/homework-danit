let usersName
let userAge //18

do {
    usersName = prompt("What's your name")
} while (usersName === "");

do {
    userAge = prompt("What's your age?");
} while (Number.isNaN(+userAge) || userAge === "" || userAge === null);

if (userAge < 18) {
    alert('You are not allowed to visit this website')
} else if (userAge >= 18 && userAge <= 22) {
    let result = confirm('Are you sure you want to continue?')
    if (result) {
        alert(`Welcome ${usersName}`)
    } else {
        alert('You are not allowed to visit this website')
    }
} else {
    alert(`Welcome ${usersName}`)
}