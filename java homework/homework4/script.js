function createNewUser() {
    const firstName = prompt('enter first name');
    const lastName = prompt('enter second name');

    return {
        firstName,
        lastName,
        getLogin: function () {
            const login = this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase()
            return login
        }
    }
}

const newUser = createNewUser();
console.log(newUser);
console.log(newUser.getLogin());